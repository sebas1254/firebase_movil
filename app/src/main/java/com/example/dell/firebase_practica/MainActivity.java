package com.example.dell.firebase_practica;

import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private String TAG;
    private FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword("sheredia@hotmail.com", "pedochino11")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "singIn:OnComplete:" + task.isSuccessful());
                        if (task.isSuccessful()) {
                            dowloadBooks();
                        } else {
                            Toast.makeText(MainActivity.this, "Sing in Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


        firebaseDatabase = firebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("mensaje");


    }

    private void dowloadBooks() {
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //para obtener instancia en la base de datos
                dataSnapshot.getValue();
                Log.e("INGRESO", "NUEVO ITEM");

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                dataSnapshot.getValue();
                Log.e("Cambio", "se cambio");

                //para cambios

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getValue();
                //borrar
                Log.e("ELIMINACION", "ELIMINAR");
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //mover
                dataSnapshot.getValue();
                Log.e("MOVER", "MOVIMIENTO");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //cancelar
                Log.e("CANCELAR", "CANCELAR");

            }
        });
    }
}



